#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 20:03:57 2020

@author: preethma
"""

import eel
from dataset_creator2 import enroll
from training_set import train
from recognize_image_dlib import recognize
from spreadsheet import spreadsh
#from imsave import saveimg

eel.init('web')

@eel.expose  
def enroll_handler_py(id,name,roll):
    enroll(id,name,roll)
    
@eel.expose
def train_handler_py():
    train()
    
    
@eel.expose
def test_handler_py(url,db):
    result=recognize(url,db)
    eel.feed_js(result)
  
@eel.expose
def report_handler_py():
    spreadsh()
    eel.report_js('rs')
    
 

eel.start('index.html', size=(1024, 768)) 
import os
import cv2
import numpy as np
from PIL import Image
import openface.openface.align_dlib as openface

dlibFacePredictor = 'shape_predictor_68_face_landmarks.dat'
recognizer = cv2.face.LBPHFaceRecognizer_create()
path = './image_dataset'
imgDimentions = 96
align = openface.AlignDlib(dlibFacePredictor)


def getImagesWithID(path):
    imageFolders = [os.path.join(path, f) for f in os.listdir(path)]

    faces = []
    Ids = []
    for imageFolder in imageFolders:
        imagePaths = [os.path.join(imageFolder, f) for f in os.listdir(imageFolder)]

        for imagePath in imagePaths:
            image = cv2.imread(imagePath)

            rgbImg = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            bb = align.getLargestFaceBoundingBox(rgbImg)

            alignedFace = align.align(imgDimentions, rgbImg, bb=None,
                                      landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
            if alignedFace is not None:
                cv2.imwrite('./training_image.jpg', alignedFace)
                faceImg = Image.open('./training_image.jpg').convert('L')
                faceNp = np.array(alignedFace, 'uint8')
                ID = int(os.path.split(imagePath)[-1].split('.')[1])

                faces.append(faceNp)
                Ids.append(ID)
                cv2.imshow("Training...", faceNp)
                cv2.waitKey(10)

    return Ids, faces

def train():
    Ids, faces = getImagesWithID(path)
    recognizer.train(faces, np.array(Ids))
    recognizer.save('./recognizer/trainingData.yml')
    cv2.destroyAllWindows()
    


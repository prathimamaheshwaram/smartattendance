from openpyxl import Workbook, load_workbook
import time
import os
import sqlite3

def spreadsh():
    #database connection
    conn = sqlite3.connect('/home/preethma/Desktop/miniProject_III_yr/Face-DataBase')
    c = conn.cursor()
    
    #get current date
    currentDate = time.strftime("%d_%m_%y")
    path = './reports/'
    date = time.strftime("%d_%m_%Y")
    if not os.path.exists(path):
        os.makedirs(path)
    
    #create a workbook and add a worksheet
    dest_filename = f'reports/{currentDate}report.xlsx'
    if(os.path.exists(f'{currentDate}report.xlsx')):
        wb = load_workbook(filename = f'{currentDate}report.xlsx')
    else:
        wb = Workbook()
        dest_filename = f'reports/{currentDate}report.xlsx'
        cmd=f"SELECT * FROM s{date} ORDER BY Roll ASC"
        c.execute(cmd)
        
        #creating worksheet and giving names to column
        ws1 = wb.active
        ws1.title = "Cse15"
        ws1.append(('Roll Number', 'Name','09_00__09_50','09_50__10_40','10_40__11_50','12_30__01_15','01_15__01_50','02_30__03_10','04_10__05_50'))
        
    
        #entering students information from database
    while True:
        a = c.fetchone()
        if a == None:
            break
        else:
            ws1.append((a[2], a[1],a[3],a[4],a[5],a[6],a[7],a[8],a[9]))
    
        #saving the file
    wb.save(filename = dest_filename)
